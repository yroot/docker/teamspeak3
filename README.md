# [Teamspeak 3](https://teamspeak.com/) Docker Image

[![pipeline status](https://gitlab.com/yroot/docker/teamspeak3/badges/master/pipeline.svg)](https://gitlab.com/yroot/docker/teamspeak3/commits/master)

## Informations

This is a docker image for a lightwight TeamSpeak 3 instance based on Alpine Linux.

## Usage

To pull the image use the following command.
```bash
docker pull registry.gitlab.com/yroot/docker/teamspeak3:latest
```

Run TeamSpeak 3 as daemon
```bash
docker run -d -p 9987:9987/udp -p 10011:10011 -p 30033:30033 --name=teamspeak3 registry.gitlab.com/yroot/docker/teamspeak3:latest
```

Run TeamSpeak 3 as daemon with volume 
```bash
docker run -d -p 9987:9987/udp -p 10011:10011 -p 30033:30033 --name=teamspeak3 -v "$(pwd)/data:/data" registry.gitlab.com/yroot/docker/teamspeak3:latest
```

## First Run

If you are starting the server for the first time, you need the generated Query Admin account and ServerAdmin-Token.

You can get the credentials in the logs:

```bash
docker logs -f teamspeak3
```

