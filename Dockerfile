FROM frolvlad/alpine-glibc

# Basic build-time metadata as defined at http://label-schema.org
LABEL maintainer="docker@yroot.io" \ 
    org.label-schema.docker.dockerfile="/Dockerfile" \
	org.label-schema.license="MIT" \
	org.label-schema.name="Teamspeak 3 Docker Image" \
	org.label-schema.url="https://gitlab.com/yroot/docker/teamspeak3" \
	org.label-schema.vcs-url="https://gitlab.com/yroot/docker/teamspeak3" \
	org.label-schema.vcs-type="Git"

ENV MIRROR "4Netplayers.de"
ENV TS_DIRECTORY "/opt/teamspeak"
ENV TS3SERVER_PARAM ""
ENV GOSU_VERSION 1.10

RUN apk update \
    && apk add --no-cache jq wget bzip2 bash ca-certificates gnupg && rm -rf /var/lib/apt/lists/* \
    && wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-amd64" \
	&& wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-amd64.asc" \
	&& export GNUPGHOME="$(mktemp -d)" \
	&& gpg --keyserver ha.pool.sks-keyservers.net --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 \
	&& gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu \
	&& rm -r "$GNUPGHOME" /usr/local/bin/gosu.asc \
	&& chmod +x /usr/local/bin/gosu \
	&& gosu nobody true \
	&& apk del gnupg

COPY init.sh /init.sh
COPY ts3server.sh /ts3server.sh

RUN mkdir -p ${TS_DIRECTORY} && \
    addgroup -g 503 teamspeak && \
	adduser -u 503 -G teamspeak -h ${TS_DIRECTORY} -D teamspeak &&\
    chown teamspeak:teamspeak ${TS_DIRECTORY} && \
	mkdir /data &&\
	chown -R teamspeak:teamspeak ${TS_DIRECTORY} /data /ts3server.sh 

VOLUME [ "/data" ]

EXPOSE 9987/udp 10011 30033
ENTRYPOINT ["/init.sh"]