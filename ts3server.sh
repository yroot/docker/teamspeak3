#!/bin/bash

set -e

cd "${TS_DIRECTORY}"

LIBRARYPATH="${TS_DIRECTORY}"
BINARYNAME="ts3server"

export LD_LIBRARY_PATH="${LIBRARYPATH}:${LD_LIBRARY_PATH}"	

ts3server_start() {
	if [ -e ts3server.pid ]; then
		if ( kill -0 $(cat ts3server.pid) 2> /dev/null ); then
			echo "The server is already running, try restart or stop"
			exit 1
		else
			echo "ts3server.pid found, but no server running. Possibly your previously started server crashed"
			echo "Please view the logfile for details."
			rm ts3server.pid
		fi
	fi
	echo "Starting the TeamSpeak 3 server"
	if [ -e "$BINARYNAME" ]; then
		if [ ! -x "$BINARYNAME" ]; then
			echo "${BINARYNAME} is not executable, trying to set it"
			chmod u+x "${BINARYNAME}"
		fi
		if [ -x "$BINARYNAME" ]; then
						
			exec "./${BINARYNAME}" ${TS3SERVER_PARAM}
			PID=$!
			ps -p ${PID} > /dev/null 2>&1
			if [ "$?" -ne "0" ]; then
				echo "TeamSpeak 3 server could not start"
			else
				echo $PID > ts3server.pid
				echo "TeamSpeak 3 server started, for details please view the log file"
			fi
		else
			echo "${BINARNAME} is not exectuable, cannot start TeamSpeak 3 server"
		fi
	else
		echo "Could not find binary, aborting"
		exit 5
	fi
}
ts3server_stop() {
	if [ -e ts3server.pid ]; then
		echo -n "Stopping the TeamSpeak 3 server"
		if ( kill -TERM $(cat ts3server.pid) 2> /dev/null ); then
			c=1
			while [ "$c" -le 30 ]; do
				if ( kill -0 $(cat ts3server.pid) 2> /dev/null ); then
					echo -n "."
					sleep 1
				else
					break
				fi
				c=$(($c+1)) 
			done
		fi
		if ( kill -0 $(cat ts3server.pid) 2> /dev/null ); then
			echo "Server is not shutting down cleanly - killing"
			kill -KILL $(cat ts3server.pid)
		else
			echo "done"
		fi
		rm ts3server.pid
	fi
}

trap ts3server_stop SIGTERM SIGKILL SIGINT

ts3server_start

exit 0