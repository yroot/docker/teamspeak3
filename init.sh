#!/bin/bash

set -e
if [ ! -d ${TS_DIRECTORY}/.ts3.lock ]; then
    echo "Download latest Teamspeak3 Server files..."
    wget -qO /tmp/ts3server.json https://www.teamspeak.com/versions/server.json
    VERSION=$(jq -r '.linux.x86_64.version' /tmp/ts3server.json)
    CHECKSUM=$(jq -r '.linux.x86_64.checksum' /tmp/ts3server.json)
    DL_URL=$(jq -r --arg MIRROR $MIRROR '.linux.x86_64.mirrors[$MIRROR]' /tmp/ts3server.json)
    rm -f /tmp/ts3server.json
    echo "VERSION: ${VERSION}"
    wget -O /tmp/ts3server.tar.bz2 $DL_URL
    
    
    echo "Check checksum:"
    echo "$CHECKSUM  /tmp/ts3server.tar.bz2" | sha256sum -c
    cd /tmp
    tar jxf ts3server.tar.bz2
    
    mv /tmp/teamspeak3-server_linux* /tmp/ts3server
    mv /tmp/ts3server/* ${TS_DIRECTORY}
    rmdir /tmp/ts3server/
    rm /tmp/ts3server.tar.bz2
    touch ${TS_DIRECTORY}/.ts3.lock
else
    echo "Lock file exists ${TS_DIRECTORY}/.ts3.lock"
fi

echo "Link data files"
if [ ! -d ${TS_DIRECTORY}/files ]; then
    # create directory for teamspeak files
    test -d /data/files || mkdir -p /data/files && chown teamspeak:teamspeak /data/files

    # create directory for teamspeak logs
    test -d /data/logs || mkdir -p /data/logs && chown teamspeak:teamspeak /data/logs

    # create symlinks for all files and directories in the persistent data directory
    cd "${TS_DIRECTORY}"
    for i in ./data/*
    do
      ln -sf "${i}"
    done

   # remove broken symlinks
   find -L "${TS_DIRECTORY}" -type l -delete

   # create symlinks for static files
   STATIC_FILES=(
      query_ip_whitelist.txt
      query_ip_blacklist.txt
      ts3server.ini
      ts3server.sqlitedb
      ts3server.sqlitedb-shm
      ts3server.sqlitedb-wal
  )
  for i in "${STATIC_FILES[@]}"
    do
      ln -sf /data/"${i}"
  done
fi

chown teamspeak:teamspeak ${TS_DIRECTORY}
chown teamspeak:teamspeak /data 

exec gosu teamspeak:teamspeak /ts3server.sh